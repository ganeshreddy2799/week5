package week5;

/**
 * 
 */

/**
 * @author jenelle.chen
 *
 */
public class FunctionsIntro {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Calling P1:
		calculateTax(100);
		
		// Calling P2:
		// ----------------------------------
		// On earth falling for 10 seconds
		calculateDistance(9.8, 10);
		// On moon, falling for 10 seconds
		calculateDistance(1.62, 10);
		// On Jupiter, falling for 10 seconds
		calculateDistance(24.79, 10);
		
	}
	
	/* P1: Write a function that calculates 
	 * the amount of tax you need to pay 
	 * 
	 * Function takes 1 parameter: price (double)
	 * Function output the total tax on the price
	 * --> output = output to console
	 */
	public static void calculateTax(double price) {
		// This will produce an ERROR:
		// WHY?  Because you are doing int/int = int
		// Therefore, TAXRATE = 0.0 --> WRONG!
		// final double TAXRATE = 13/100;
		
		//  TO FIX: Make sure ONE of the numbers is a double
		final double TAXRATE = 13.0/100.0;

		double result = price * TAXRATE;
		System.out.println("Tax is: " + result);
	}
	
	
	// P2:  Write a function that calculates 
	// the distance that an object falls down (under gravity)
	// d = 0.5*g*t    // g = gravity (9.8m/s^2)
	// t = time 	  - how long its falling for
	
	// Your function 2 parameters:
	// -- parameter 1  is gravity
	// -- parameter 2 is time (in seconds)
	// Your function should OUTPUT the result to console
	public static void calculateDistance(double g, int t) {
		double distance = 0.5*g*t;
		System.out.println("Distance travelled: " + distance  + " meters");
	}

	

}
